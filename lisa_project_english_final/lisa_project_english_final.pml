<?xml version="1.0" encoding="UTF-8" ?>
<Package name="lisa_project_english_final" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs>
        <Dialog name="ExampleDialog" src="behavior_1/ExampleDialog/ExampleDialog.dlg" />
    </Dialogs>
    <Resources>
        <File name="choice_sentences" src="behavior_1/Aldebaran/choice_sentences.xml" />
        <File name="Thriller-music-mp3" src="Thriller-music-mp3.mp3" />
        <File name="highwayToHellNao" src="highwayToHellNao.mp3" />
        <File name="Bill Conti - Gonna Fly Now (Theme From Rocky)" src="Bill Conti - Gonna Fly Now (Theme From Rocky).mp3" />
        <File name="choice_sentences_light" src="behavior_1/Aldebaran/choice_sentences_light.xml" />
        <File name="swiftswords_ext" src="behavior_1/swiftswords_ext.mp3" />
        <File name="Walking on Sunshine_nao" src="Walking on Sunshine_nao.mp3" />
        <File name="eye of the tiger_nao" src="eye of the tiger_nao.mp3" />
    </Resources>
    <Topics>
        <Topic name="ExampleDialog_enu" src="behavior_1/ExampleDialog/ExampleDialog_enu.top" topicName="ExampleDialog" language="en_US" />
    </Topics>
    <IgnoredPaths />
</Package>
