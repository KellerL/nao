import random

# Functions
def calcRandomIndex():
    amountWords = len(choosenLanguage) # Amount of words in array which holds the vocabulary
    index = random.randrange(amountWords)  # Generate random number between 0 and amountWords - 1 for index of lists
    return index

# Declaration of variables
german = ['rot', 'Schlange', 'Lied', 'Roboter', 'tanzen', 'Hund']
english = ['red', 'snake', 'song', 'robot', 'dance', 'dog']
choosenLanguage = []
otherLanguage = []
otherLanguageString = ''

# "start" of actual program
userInput = input('Do you like to train words in English or German?')

if str(userInput) == 'German':
    choosenLanguage = german
    otherLanguage = english
    otherLanguageString = 'English'
elif str(userInput) == 'English':
    choosenLanguage = english
    otherLanguage = german
    otherLanguageString = 'German'
else:
    print('Please check your spelling.')


while len(choosenLanguage) > 0: # asks vocabulary until there is not a single word left in the list
    index = calcRandomIndex()
    variable = input('What is ' + choosenLanguage[index] + ' in ' + otherLanguageString + '?')
    if variable == otherLanguage[index]:
        print('Great, that is right!')
        del choosenLanguage[index]
        del otherLanguage[index]
    else:
        print('That is not right, unfortunately.')



